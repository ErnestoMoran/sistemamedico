package org.backendproyecto.model;

import java.util.ArrayList;

public class FichaDes {

    private Ficha ficha;
    private ArrayList<Motivo> motivos;

    public Ficha getFicha() {
        return ficha;
    }

    public void setFicha(Ficha ficha) {
        this.ficha = ficha;
    }

    public ArrayList<Motivo> getMotivos() {
        return motivos;
    }

    public void setMotivos(ArrayList<Motivo> motivos) {
        this.motivos = motivos;
    }
}
