package org.backendproyecto.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "medico")
public class Medico {
    
    @Id
    @Column(name = "id_medico")
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idMedicoSec")
    @SequenceGenerator(name = "idMedicoSec",sequenceName = "id_medico_sec",allocationSize = 0)
    private Integer id_medico;

    @Column(name = "nombre_medico")
    @Basic(optional = false)
    private String nombre_medico;

    @Column(name = "apellido_medico")
    @Basic(optional = false)
    private String apellido_medico;

    @Column(name = "cedula_medico",unique = true)
    @Basic(optional = false)
    private Integer cedula_medico;

    @Column(name = "email_medico")
    @Basic(optional = false)
    private String email_medico;

    @Column(name = "telefono_medico")
    @Basic(optional = false)
    private String telefono_medico;

    @Column(name = "fecha_medico")
    @Basic(optional = false)
    private String fecha_medico;

    @Column(name = "especialidad")
    @Basic(optional = false)
    private String especialidad;

    @Column(name = "usuario")
    @Basic(optional = false)
    private String usuario;

    @Column(name = "password")
    @Basic(optional = false)
    private String password;

    @JsonIgnore
    @OneToMany(mappedBy = "medico",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Set<Ficha> medicoF;

    public Medico() {
    }

    public Integer getId_medico() {
        return id_medico;
    }

    public void setId_medico(Integer id_medico) {
        this.id_medico = id_medico;
    }

    public String getNombre_medico() {
        return nombre_medico;
    }

    public void setNombre_medico(String nombre_medico) {
        this.nombre_medico = nombre_medico;
    }

    public String getApellido_medico() {
        return apellido_medico;
    }

    public void setApellido_medico(String apellido_medico) {
        this.apellido_medico = apellido_medico;
    }

    public Integer getCedula_medico() {
        return cedula_medico;
    }

    public void setCedula_medico(Integer cedula_medico) {
        this.cedula_medico = cedula_medico;
    }

    public String getEmail_medico() {
        return email_medico;
    }

    public void setEmail_medico(String email_medico) {
        this.email_medico = email_medico;
    }

    public String getTelefono_medico() {
        return telefono_medico;
    }

    public void setTelefono_medico(String telefono_medico) {
        this.telefono_medico = telefono_medico;
    }

    public String getFecha_medico() {
        return fecha_medico;
    }

    public void setFecha_medico(String fecha_medico) {
        this.fecha_medico = fecha_medico;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Ficha> getMedicoF() {
        return medicoF;
    }

    public void setMedicoF(Set<Ficha> medicoF) {
        this.medicoF = medicoF;
    }
}
