package org.backendproyecto.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "paciente")
public class Paciente {

    @Id
    @Column(name = "id_paciente")
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idPacienteSec")
    @SequenceGenerator(name = "idPacienteSec",sequenceName = "id_paciente_sec",allocationSize = 0)
    private Integer id_paciente;

    @Column(name = "nombre_paciente")
    @Basic(optional = false)
    private String nombre_paciente;

    @Column(name = "apellido_paciente")
    @Basic(optional = false)
    private String apellido_paciente;

    @Column(name = "cedula_paciente",unique = true)
    @Basic(optional = false)
    private Integer cedula_paciente;

    @Column(name = "email_paciente")
    @Basic(optional = false)
    private String email_paciente;

    @Column(name = "telefono_paciente")
    @Basic(optional = false)
    private String telefono_paciente;

    @Column(name = "fecha_paciente")
    @Basic(optional = false)
    private String fecha_paciente;

    @JsonIgnore
    @OneToMany(mappedBy = "paciente",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Set<Ficha> pacienteF;

    public Paciente() {
    }

    public Integer getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(Integer id_paciente) {
        this.id_paciente = id_paciente;
    }

    public String getNombre_paciente() {
        return nombre_paciente;
    }

    public void setNombre_paciente(String nombre_paciente) {
        this.nombre_paciente = nombre_paciente;
    }

    public String getApellido_paciente() {
        return apellido_paciente;
    }

    public void setApellido_paciente(String apellido_paciente) {
        this.apellido_paciente = apellido_paciente;
    }

    public Integer getCedula_paciente() {
        return cedula_paciente;
    }

    public void setCedula_paciente(Integer cedula_paciente) {
        this.cedula_paciente = cedula_paciente;
    }

    public String getEmail_paciente() {
        return email_paciente;
    }

    public void setEmail_paciente(String email_paciente) {
        this.email_paciente = email_paciente;
    }

    public String getTelefono_paciente() {
        return telefono_paciente;
    }

    public void setTelefono_paciente(String telefono_paciente) {
        this.telefono_paciente = telefono_paciente;
    }

    public String getFecha_paciente() {
        return fecha_paciente;
    }

    public void setFecha_paciente(String fecha_paciente) {
        this.fecha_paciente = fecha_paciente;
    }

    public Set<Ficha> getPacienteF() {
        return pacienteF;
    }

    public void setPacienteF(Set<Ficha> pacienteF) {
        this.pacienteF = pacienteF;
    }
}
