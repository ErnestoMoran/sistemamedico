package org.backendproyecto.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "ficha")
public class Ficha {


    @Id
    @Column(name = "id_ficha")
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idFichaSec")
    @SequenceGenerator(name = "idFichaSec",sequenceName = "id_ficha_sec",allocationSize = 0)
    private Integer id_ficha;

    @Column(name = "fecha_actual",nullable = true)
    @Basic(optional = false)
    private String fecha_actual;

    @JoinColumn(name = "paciente_ficha", columnDefinition = "id_paciente",nullable = true)
    @ManyToOne(optional = false,fetch =  FetchType.EAGER)
    private Paciente paciente;

    @JsonIgnore
    @OneToMany(mappedBy = "ficha",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Set<Motivo> detalleConsulta;

    @JoinColumn(name = "medico_ficha",columnDefinition = "id_medico",nullable = true)
    @ManyToOne(optional = false,fetch =  FetchType.EAGER)
    private Medico medico;

    public Ficha() {
    }

    public Integer getId_ficha() {
        return id_ficha;
    }

    public void setId_ficha(Integer id_ficha) {
        this.id_ficha = id_ficha;
    }

    public String  getFecha_actual() {
        return fecha_actual;
    }

    public void setFecha_actual(String fecha_actual) {
        this.fecha_actual = fecha_actual;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Set<Motivo> getDetalleConsulta() {
        return detalleConsulta;
    }

    public void setDetalleConsulta(Set<Motivo> detalleConsulta) {
        this.detalleConsulta = detalleConsulta;
    }
}
