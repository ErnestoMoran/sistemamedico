package org.backendproyecto.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table( name = "motivo")
public class Motivo {

    @Id
    @Column(name = "id_motivo")
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idMotivoSec")
    @SequenceGenerator(name = "idMotivoSec",sequenceName = "id_motivo_sec",allocationSize = 0)
    private Integer id_motivo;

    @Column(name = "texto_motivo")
    @Basic(optional = false)
    private String texto_motivo;

    @Column(name = "diagnostico")
    @Basic(optional = false)
    private String diagnostico;

    @Column(name = "tratamiento")
    @Basic(optional = false)
    private String tratamiento;

    @JoinColumn(name = "id_ficha",columnDefinition = "id_ficha")
    @ManyToOne(optional = false,fetch =  FetchType.EAGER)
    private Ficha ficha;

    public Motivo() {
    }

    public Integer getId_motivo() {
        return id_motivo;
    }

    public void setId_motivo(Integer id_motivo) {
        this.id_motivo = id_motivo;
    }

    public String getTexto_motivo() {
        return texto_motivo;
    }

    public void setTexto_motivo(String texto_motivo) {
        this.texto_motivo = texto_motivo;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public Ficha getFicha() {
        return ficha;
    }

    public void setFicha(Ficha ficha) {
        this.ficha = ficha;
    }
}
