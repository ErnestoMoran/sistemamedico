package org.backendproyecto.model;

import org.hibernate.mapping.Array;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class JoinFichas {
    private Integer id_ficha;
    private Date fecha_actual;
    private String nombre_paciente;
    private String apellido_paciente;
    private String nombre_medico;
    private String apellido_medico;
    private List<JoinMotivos> detalleConsulta;

    public Integer getId_ficha() {
        return id_ficha;
    }

    public void setId_ficha(Integer id_ficha) {
        this.id_ficha = id_ficha;
    }

    public Date getFecha_actual() {
        return fecha_actual;
    }

    public void setFecha_actual(Date fecha_actual) {
        this.fecha_actual = fecha_actual;
    }

    public String getNombre_paciente() {
        return nombre_paciente;
    }

    public void setNombre_paciente(String nombre_paciente) {
        this.nombre_paciente = nombre_paciente;
    }

    public String getApellido_paciente() {
        return apellido_paciente;
    }

    public void setApellido_paciente(String apellido_paciente) {
        this.apellido_paciente = apellido_paciente;
    }

    public String getNombre_medico() {
        return nombre_medico;
    }

    public void setNombre_medico(String nombre_medico) {
        this.nombre_medico = nombre_medico;
    }

    public String getApellido_medico() {
        return apellido_medico;
    }

    public void setApellido_medico(String apellido_medico) {
        this.apellido_medico = apellido_medico;
    }

    public List<JoinMotivos> getDetalleConsulta() {
        return detalleConsulta;
    }

    public void setDetalleConsulta(List<JoinMotivos> detalleConsulta) {
        this.detalleConsulta = detalleConsulta;
    }
}
