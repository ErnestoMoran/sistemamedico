package org.backendproyecto.model;

public class JoinMotivos {

    private String texto_motivo;
    private String diagnostico;
    private String tratamiento;
    private Integer id_motivo;
    private Integer id_ficha;

    public String getTexto_motivo() {
        return texto_motivo;
    }

    public void setTexto_motivo(String texto_motivo) {
        this.texto_motivo = texto_motivo;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public Integer getId_motivo() {
        return id_motivo;
    }

    public void setId_motivo(Integer id_motivo) {
        this.id_motivo = id_motivo;
    }

    public Integer getId_ficha() {
        return id_ficha;
    }

    public void setId_ficha(Integer id_ficha) {
        this.id_ficha = id_ficha;
    }
}
