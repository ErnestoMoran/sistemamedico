package org.backendproyecto.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.backendproyecto.ejb.FichaDao;
import org.backendproyecto.ejb.MotivoDao;
import org.backendproyecto.model.Ficha;
import org.backendproyecto.model.FichaDes;
import org.backendproyecto.model.Motivo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.Date;
import java.util.ArrayList;

@Path("ficha")
@Consumes("application/json")
@Produces("application/json")
public class FichaRest {

    @Inject
    private FichaDao fichaDao;

    @Inject
    private MotivoDao motivoDao;

    private final static Logger log= LoggerFactory.getLogger(PacienteRest.class);

    @POST
    @Path("/crear")
    public Response crear(FichaDes f) {

        Ficha ficha=f.getFicha();
        ficha.setDetalleConsulta(null);
        ficha.setId_ficha(null);
        ArrayList<Motivo> motivos=f.getMotivos();
        try {
            Ficha new_ficha=this.fichaDao.agregar(ficha);
            for(Motivo m:motivos){
                m.setId_motivo(null);
                m.setFicha(new_ficha);
                motivoDao.agregar(m);
            }
            return Response.ok("Se ha creado la ficha con exito").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }

    }

    @GET
    @Path("/listar")
    public Response getFichas(){
        try {
            return Response.ok(this.fichaDao.listar()).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/listarPorNombre/{nombrePaciente}")
    public Response getFichas(@PathParam(value = "nombrePaciente") String paciente){
        try {
            return Response.ok(this.fichaDao.listarPorNombre(paciente)).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/listarPorMedico/{nombreMedico}")
    public Response getFichasMedico(@PathParam(value = "nombreMedico") String medico){
        try {
            return Response.ok(this.fichaDao.listarPorMedico(medico)).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().build();
        }
    }


    @GET
    @Path("/listarPorDiagnostico/{diagnostico}")
    public Response getFichasPorDiagnostico(@PathParam(value = "diagnostico") String diagnostico){
        try {
            return Response.ok(this.fichaDao.listarPorDiagnostico(diagnostico)).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/listarPorEspecialidad/{especialidad}")
    public Response getFichasEspecilidad(@PathParam(value = "especialidad") String especialidad){
        try {
            return Response.ok(this.fichaDao.listarPorEspecialidad(especialidad)).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().build();
        }
    }
}
