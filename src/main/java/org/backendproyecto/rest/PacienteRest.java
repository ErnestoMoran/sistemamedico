package org.backendproyecto.rest;

import org.backendproyecto.ejb.PacienteDao;
import org.backendproyecto.model.Paciente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("paciente")
@Consumes({"application/json", MediaType.MULTIPART_FORM_DATA,MediaType.APPLICATION_FORM_URLENCODED})
@Produces("application/json")
public class PacienteRest {
    @Inject
    private PacienteDao pacienteDao;

    private final static Logger log= LoggerFactory.getLogger(PacienteRest.class);

    @GET
    @Path("/")
    public Response listar() {
        return Response.ok(pacienteDao.listaPacientes()).header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
    }

    @POST
    @Path("/crear")
    public Response crear(Paciente p) {
        try {
            this.pacienteDao.agregar(p);
            return Response.ok("Se ha creado el paciente con exito").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @PUT
    @Path("/modificar/{idEntidad}")
    public Response modificarPaciente(@PathParam("idEntidad") Integer id, Paciente entidad){
        try {
            this.pacienteDao.modificarPaciente(entidad, id);
            return Response.ok("Se ha modificado el paciente con exito").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @DELETE
    @Path("/eliminar/{idPaciente}")
    public Response eliminarCliente(@PathParam(value = "idPaciente") Integer idPaciente){
        try {
            this.pacienteDao.eliminarPaciente(idPaciente);
            return Response.ok("Se ha eliminado el paciente correctamente").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @GET
    @Path("/{nombre}/{apellido}")
    public Response buscarPacienteNombreApellido(@PathParam(value = "nombre") String nombre, @PathParam(value = "apellido") String apellido){
        log.info("Buscando nombre:"+nombre+" y apellido:"+apellido);
        try{
            return Response.ok(this.pacienteDao.listaPorNombreApellido(nombre,apellido)).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @GET
    @Path("buscarPaciente/{cedulaPaciente}")
    public Response buscarPacienteCedula(@PathParam(value = "cedulaPaciente") Integer cedulaPaciente){
        log.info("Buscando cedula:"+cedulaPaciente);
        try{
            return Response.ok(this.pacienteDao.buscarPorCedula(cedulaPaciente)).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @DELETE
    @Path("eliminarPorCI/{cedulaPaciente}")
    public Response eliminarPorCedula(@PathParam(value = "cedulaPaciente") Integer cedulaPaciente){
        try{
            this.pacienteDao.eliminarPorCedula(cedulaPaciente);
            return Response.ok("El paciente se ha eliminado con exito").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }
}
