package org.backendproyecto.rest;

import org.backendproyecto.ejb.MedicoDao;
import org.backendproyecto.model.Medico;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("medico")
@Consumes("application/json")
@Produces("application/json")
public class MedicoRest {

    @Inject
    private MedicoDao medicoDao;

    private final static Logger log= LoggerFactory.getLogger(MedicoRest.class);

    @GET
    @Path("/")
    public Response listar() {
        return Response.ok(medicoDao.listaMedicos()).header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
    }

    @POST
    @Path("/crear")
    public Response crear(Medico m) {
        try {
            this.medicoDao.agregar(m);
            return Response.ok("Se ha creado el medico con exito").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @PUT
    @Path("/modificar/{idEntidad}")
    public Response modificarMedico(@PathParam("idEntidad") Integer id, Medico entidad){
        try {
            this.medicoDao.modificarMedico(entidad, id);
            return Response.ok("Se ha modificado el medico con exito").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @DELETE
    @Path("/eliminar/{idMedico}")
    public Response eliminarMedico(@PathParam(value = "idMedico") Integer idMedico){
        try {
            this.medicoDao.eliminarMedico(idMedico);
            return Response.ok("Se ha eliminado el medico correctamente").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @GET
    @Path("/{usuario}/{password}")
    public Response listarPorUsuario(@PathParam(value = "usuario") String usuario, @PathParam(value = "password") String password) {

        Medico medic= medicoDao.seleccionMedico(usuario,password);

        return Response.status(Response.Status.OK).header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").entity(medic).build();
    }

    @GET
    @Path("buscarMedico/{cedulaMedico}")
    public Response buscarPacienteCedula(@PathParam(value = "cedulaMedico") Integer cedulaMedico){
        log.info("Buscando cedula:"+cedulaMedico);
        try{
            return Response.ok(this.medicoDao.buscarPorCedula(cedulaMedico)).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @DELETE
    @Path("eliminarPorCI/{cedulaMedico}")
    public Response eliminarPorCedula(@PathParam(value = "cedulaMedico") Integer cedulaMedico){
        try{
            this.medicoDao.eliminarPorCedula(cedulaMedico);
            return Response.ok("El medico se ha eliminado con exito").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

}
