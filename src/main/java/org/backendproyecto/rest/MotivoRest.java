package org.backendproyecto.rest;

import org.backendproyecto.ejb.MotivoDao;
import org.backendproyecto.model.Motivo;
import org.backendproyecto.model.Paciente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.Date;

@Path("motivo")
@Consumes("application/json")
@Produces("application/json")
public class MotivoRest {

    @Inject
    private MotivoDao motivoDao;

    private final static Logger log= LoggerFactory.getLogger(PacienteRest.class);


    @POST
    @Path("/crear")
    public Response crear(Motivo m) {
        try {
            this.motivoDao.agregar(m);
            return Response.ok("Se ha creado el detalle de la consulta con exito").header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

    @GET
    @Path("filtrarPor/{nombrePa}")
    public Response filtradoMotivo(@PathParam(value = "nombrePa") String nombrePa){
        try{
            return Response.ok(this.motivoDao.filtrarDetalles("","","",nombrePa,"","")).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
        log.error(e.toString());
        return Response.serverError().header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
    }
    }

    @GET
    @Path("listarPorIdFicha/{idFicha}")
    public Response listarPorFicha(@PathParam(value = "idFicha") Integer idFicha){
        try{
            return Response.ok(this.motivoDao.filtrarPorFichaId(idFicha)).header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }catch (Exception e) {
            log.error(e.toString());
            return Response.serverError().header("Access-Control-Allow-Origin","*")
                    .header("Access-Control-Allow-Credentials","true").header("Access-Control-Allow-Headers","origin, content-type, accept, authorizarion")
                    .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
        }
    }

}
