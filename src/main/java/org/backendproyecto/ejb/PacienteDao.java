package org.backendproyecto.ejb;

import org.backendproyecto.model.Paciente;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Stateless
public class PacienteDao {

    @PersistenceContext(unitName = "sistemaMedico")
    private EntityManager em;

    public void agregar(Paciente entidad){
        this.em.persist(entidad);
    }

    public void modificarPaciente( Paciente entidad, Integer id){
        Paciente paciente= new Paciente();
        paciente= this.em.find(Paciente.class,id);
        paciente.setNombre_paciente(entidad.getNombre_paciente());
        paciente.setApellido_paciente(entidad.getApellido_paciente());
        paciente.setCedula_paciente(entidad.getCedula_paciente());
        paciente.setEmail_paciente(entidad.getEmail_paciente());
        paciente.setTelefono_paciente(entidad.getTelefono_paciente());
        paciente.setFecha_paciente(entidad.getFecha_paciente());
        this.em.merge(paciente);
    }

    public void eliminarPaciente(Integer id){
        Paciente paciente = new Paciente();
        paciente=this.em.find(Paciente.class,id);
        this.em.remove(paciente);
    }

    public List<Paciente> listaPacientes(){
        Query q=this.em.createNativeQuery("SELECT * FROM paciente",Paciente.class);
        return (List<Paciente>) q.getResultList();
    }

    public List<Paciente> listaPorNombreApellido(String nombre, String apellido){
        Query q=this.em.createNativeQuery("SELECT * FROM paciente WHERE nombre_paciente ILIKE :nombre AND apellido_paciente ILIKE :apellido",Paciente.class);
        q.setParameter("nombre",nombre+"%");
        q.setParameter("apellido",apellido+"%");
        return (List<Paciente>) q.getResultList();
    }

    public Object buscarPorCedula(Integer cedulaPaciente) {
        Query q=this.em.createNativeQuery("SELECT * FROM paciente WHERE cedula_paciente = :cedula_paciente",Paciente.class);
        q.setParameter("cedula_paciente",cedulaPaciente);
        return (Paciente) q.getSingleResult();
    }

    public void eliminarPorCedula(Integer cedulaPaciente) {
        Query q=this.em.createNativeQuery("DELETE FROM paciente WHERE cedula_paciente = :cedula_paciente");
        q.setParameter("cedula_paciente",cedulaPaciente);
        q.executeUpdate();
    }
}
