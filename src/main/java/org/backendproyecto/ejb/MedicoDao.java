package org.backendproyecto.ejb;


import org.backendproyecto.model.Medico;
import org.backendproyecto.model.Paciente;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class MedicoDao {
    @PersistenceContext(unitName = "sistemaMedico")
    private EntityManager em;

    public void agregar(Medico entidad){
        this.em.persist(entidad);
    }

    public void modificarMedico( Medico entidad, Integer id){
        Medico medico= new Medico();
        medico= this.em.find(Medico.class,id);
        medico.setNombre_medico(entidad.getNombre_medico());
        medico.setApellido_medico(entidad.getApellido_medico());
        medico.setCedula_medico(entidad.getCedula_medico());
        medico.setEmail_medico(entidad.getEmail_medico());
        medico.setTelefono_medico(entidad.getTelefono_medico());
        medico.setFecha_medico(entidad.getFecha_medico());
        this.em.merge(medico);
    }

    public void eliminarMedico(Integer id){
        Medico medico = new Medico();
        medico=this.em.find(Medico.class,id);
        this.em.remove(medico);
    }

    public List<Medico> listaMedicos(){
        Query q=this.em.createNativeQuery("SELECT * FROM medico",Medico.class);
        return (List<Medico>) q.getResultList();
    }

    public Medico  seleccionMedico(String usuario, String password){
        Query q=this.em.createNativeQuery("SELECT * FROM medico WHERE usuario = :usuario AND password = :password",Medico.class);
        q.setParameter("usuario",usuario);
        q.setParameter("password",password);
        return (Medico) q.getSingleResult();
    }

    public Object buscarPorCedula(Integer cedulaMedico) {
        Query q=this.em.createNativeQuery("SELECT * FROM medico WHERE cedula_medico = :cedula_medico", Medico.class);
        q.setParameter("cedula_medico",cedulaMedico);
        return (Medico) q.getSingleResult();
    }

    public void eliminarPorCedula(Integer cedulaMedico) {
        Query q=this.em.createNativeQuery("DELETE FROM medico WHERE cedula_medico = :cedula_medico");
        q.setParameter("cedula_medico",cedulaMedico);
        q.executeUpdate();
    }
}
