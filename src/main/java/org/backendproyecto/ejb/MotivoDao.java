package org.backendproyecto.ejb;

import org.backendproyecto.model.Motivo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Date;
import java.util.List;

@Stateless
public class MotivoDao {

    @PersistenceContext(unitName = "sistemaMedico")
    private EntityManager em;

    public void agregar(Motivo entidad){
        this.em.persist(entidad);
    }

    public List<Object> filtrarDetalles(String textoM, String diagnostico, String tratamiento, String nombrePac, String nombreMed, String especialidadMe){
        Query q=this.em.createNativeQuery("SELECT texto_motivo,diagnostico,tratamiento,nombre_paciente,nombre_medico,especialidad,fecha_actual FROM motivo " +
                "INNER JOIN  ficha ON motivo.id_ficha = ficha.id_ficha" +
                " INNER JOIN  paciente ON ficha.paciente_ficha = paciente.id_paciente" +
                " INNER JOIN  medico ON ficha.medico_ficha = medico.id_medico" +
                " WHERE nombre_paciente = :nombrePac OR nombre_medico = :nombreMed OR especialidad = :especialidadMe " +
                " OR texto_motivo = :textoM OR diagnostico = :diagnostico OR tratamiento = :tratamiento");
        q.setParameter("nombrePac",nombrePac);
        q.setParameter("nombreMed",nombreMed);
        q.setParameter("especialidadMe",especialidadMe);
        q.setParameter("textoM",textoM);
        q.setParameter("diagnostico",diagnostico);
        q.setParameter("tratamiento",tratamiento);

        return (List<Object>) q.getResultList();
    }

    public List<Motivo> filtrarPorFichaId( Integer idFicha){
        Query q=this.em.createNativeQuery("SELECT * FROM motivo" +
                " WHERE id_ficha = :idFicha ",Motivo.class);
        q.setParameter("idFicha",idFicha);


        return (List<Motivo>) q.getResultList();
    }
}
