package org.backendproyecto.ejb;

import org.backendproyecto.model.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Stateless
public class FichaDao {

    @PersistenceContext(unitName = "sistemaMedico")
    private EntityManager em;

    @Inject
    private MotivoDao motivoDao;

    public Ficha agregar(Ficha entidad) {
        this.em.persist(entidad);
        this.em.flush();
        return entidad;
    }

    public List<Object> listarDetalles() {
        Query q = this.em.createNativeQuery("SELECT ficha.id_ficha,fecha_actual,nombre_paciente," +
                "apellido_paciente,nombre_medico,apellido_medico FROM ficha " +
                "INNER JOIN  motivo ON motivo.id_ficha = ficha.id_ficha" +
                " INNER JOIN  paciente ON ficha.paciente_ficha = paciente.id_paciente" +
                " INNER JOIN  medico ON ficha.medico_ficha = medico.id_medico " +
                "GROUP BY ficha.id_ficha,paciente.nombre_paciente,paciente.apellido_paciente,medico.nombre_medico,medico.apellido_medico",Ficha.class);


        return q.getResultList();
    }

    public List<Ficha> listar(){
        Query q=this.em.createNativeQuery("SELECT * FROM ficha",Ficha.class);

        return (List<Ficha>) q.getResultList();
    }

    public List<Ficha> listarPorNombre(String nombre_paciente) {
        Query q=this.em.createNativeQuery("SELECT * FROM ficha INNER JOIN  paciente ON ficha.paciente_ficha = paciente.id_paciente " +
                "WHERE nombre_paciente ILIKE :nombre_paciente",Ficha.class);
        q.setParameter("nombre_paciente", nombre_paciente+"%");

        return q.getResultList();
    }

    public List<Ficha> listarPorMedico(String nombre_medico) {
        Query q=this.em.createNativeQuery("SELECT * FROM ficha INNER JOIN  medico ON ficha.medico_ficha = medico.id_medico " +
                "WHERE nombre_medico ILIKE :nombre_medico",Ficha.class);
        q.setParameter("nombre_medico", nombre_medico+"%");

        return q.getResultList();
    }

    public List<Ficha> listarPorEspecialidad(String especialidad) {
        Query q=this.em.createNativeQuery("SELECT * FROM ficha INNER JOIN  medico ON ficha.medico_ficha = medico.id_medico " +
                "WHERE especialidad ILIKE :especialidad",Ficha.class);
        q.setParameter("especialidad", especialidad+"%");

        return q.getResultList();
    }

    public List<Ficha> listarPorDiagnostico(String diagnostico) {
        Query q=this.em.createNativeQuery("SELECT * FROM ficha INNER JOIN  motivo ON ficha.id_ficha = motivo.id_ficha " +
                "WHERE diagnostico ILIKE :diagnostico",Ficha.class);
        q.setParameter("diagnostico", diagnostico+"%");

        return q.getResultList();
    }

}
/*
* SELECT ficha.id_ficha,fecha_actual,nombre_paciente,apellido_paciente,nombre_medico,apellido_medico,texto_motivo,diagnostico,tratamiento FROM public.ficha
                INNER JOIN  public.motivo ON motivo.id_ficha = ficha.id_ficha
                 INNER JOIN  public.paciente ON ficha.paciente_ficha = paciente.id_paciente
                 INNER JOIN  public.medico ON ficha.medico_ficha = medico.id_medico;
* */

/*SELECT fecha_actual,nombre_paciente,apellido_paciente,nombre_medico,apellido_medico,array_agg(motivo) AS detalleConsulta FROM public.ficha
                INNER JOIN  public.motivo ON motivo.id_ficha = ficha.id_ficha
                 INNER JOIN  public.paciente ON ficha.paciente_ficha = paciente.id_paciente
                 INNER JOIN  public.medico ON ficha.medico_ficha = medico.id_medico
                 GROUP BY ficha.id_ficha,paciente.nombre_paciente,paciente.apellido_paciente,medico.nombre_medico,medico.apellido_medico;
                 */